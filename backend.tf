terraform {
  backend "s3" {
    bucket = "terraform-backup-files"
    region = "us-east-1"
  }
}
