data "aws_route53_zone" "primary" {
  name = var.hosted_zone_name
}

resource "aws_route53_record" "subdomain" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = local.website_url
  # name    = length(var.dns_record_name) > 0 ? var.dns_record_name : "${var.project_name}.${var.environment}.${var.hosted_zone_name}"
  type    = "A"

  alias {
    name = aws_cloudfront_distribution.website.domain_name
    zone_id = aws_cloudfront_distribution.website.hosted_zone_id
    evaluate_target_health = false
  }
}

output "public-url" {
  value = "https://${aws_route53_record.subdomain.fqdn}/"
}