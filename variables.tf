variable "project_group" {
  description = "Project group (ex: jcavard)."
}

variable "project_name" {
  description = "Project name (ex: website)"
}

variable "project_key" {
  description = "Project key (ex: PROJ)"
}

variable "aws_region" {
  description = "The AWS region to create resources in."
}

variable "environment" {
  description = "The environment name."
}

variable "hosted_zone_name" {
  description = "The existing AWS hosted zone to be used."
}

variable "dns_record_name" {
  description = "The url the website."
  default = ""
}

variable "cache_default_ttl" {
  description = "Cloudfront's cache default time to live."
  default = 3600
}

variable "cache_max_ttl" {
  description = "Cloudfront's cache maximun time to live."
  default = 86400
}

variable "certificate_domain_name" {
    description = "Domain of an amazon issued certificate to be used."
}