locals {
  s3_bucket_name = lower(join(".", [var.project_group, var.project_name, var.environment, "content"]))
  origin_id = lower(join("-", [var.project_group, var.project_name, var.environment]))
  
  website_url = length(var.dns_record_name) > 0 ? var.dns_record_name : join(".", [var.project_name, var.environment, var.hosted_zone_name])

  # Common tags to attach to resources
  common_tags = {
    Environment = lower(var.environment)
    ProjectGroup = lower(var.project_group)
    ProjectKey  = upper(var.project_key)
    ProjectName = lower(var.project_name)
  }
}