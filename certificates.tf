data "aws_acm_certificate" "root-certificate" {
  domain   = var.certificate_domain_name
  provider = aws.main
  statuses = ["ISSUED"]
  types       = ["AMAZON_ISSUED"]
}