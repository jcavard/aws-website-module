# README #

Requires the following resources to be already created
* certificates
* hosted zone name

Creates the following resources
* route 53 
* s3 bucket format `<project-group>.<project-name>.<environment>.content`
* cloudfront distribution
